const Sequelize = require('sequelize')
const { db } = require('../config/database')
const { Presupuestos } = require('./presupuestos')
const { Productos } = require('./productos')

var PresupuestosDetalle = db.sequelize.define(
    'presupuestos_detalle',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        presupuesto: {
            type: Sequelize.INTEGER
        },
        producto: {
            type: Sequelize.INTEGER
        },
        cantidad: {
            type: Sequelize.INTEGER
        },
        precio_unitario: {
            type: Sequelize.NUMBER
        },
        total: {
            type: Sequelize.NUMBER
        }
    }, {
        freezeTableName: true
    }
)

PresupuestosDetalle.hasOne(Productos, { foreignKey: 'id', sourceKey: 'producto' , as: 'Producto' })

module.exports = {PresupuestosDetalle}