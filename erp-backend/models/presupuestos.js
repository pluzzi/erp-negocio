const Sequelize = require('sequelize')
const { db } = require('../config/database')
const { Clientes } = require('./clientes')
const { PresupuestosDetalle } = require('./presupuestos_detalle')

var Presupuestos = db.sequelize.define(
    'presupuestos',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        cliente: {
            type: Sequelize.INTEGER
        }
    }, {
        freezeTableName: true
    }
)

Presupuestos.hasOne(Clientes, { foreignKey: 'id', sourceKey: 'cliente' , as: 'Cliente' })
Presupuestos.hasMany(PresupuestosDetalle, { foreignKey: 'presupuesto', targetKey: 'id', as: 'Presupuesto_Detalle' })

module.exports = {Presupuestos}