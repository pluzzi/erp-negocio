const Sequelize = require('sequelize')
const { db } = require('../config/database')
const { Proveedores } = require('./proveedores')
const { Monedas } = require('./monedas')
const { CategoriasProducto } = require('./categorias_productos')

var Productos = db.sequelize.define(
    'productos',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        producto: {
            type: Sequelize.STRING
        },
        categoria_producto: {
            type: Sequelize.INTEGER
        },
        codigo: {
            type: Sequelize.STRING
        },
        marca: {
            type: Sequelize.STRING
        },
        descripcion: {
            type: Sequelize.STRING
        },
        proveedor:{
            type: Sequelize.INTEGER
        },
        precio_unitario: {
            type: Sequelize.NUMBER
        },
        iva: {
            type: Sequelize.NUMBER
        },
        por_gan: {
            type: Sequelize.NUMBER
        },
        precio_final: {
            type: Sequelize.NUMBER
        },
        moneda: {
            type: Sequelize.INTEGER
        }
    }, {
        freezeTableName: true
    }
)

Productos.hasOne(Proveedores, { foreignKey: 'id', sourceKey: 'proveedor', as: 'Proveedor' })
Productos.hasOne(Monedas, { foreignKey: 'id', sourceKey: 'moneda', as: 'Moneda' })
Productos.hasOne(CategoriasProducto, { foreignKey: 'id', sourceKey: 'categoria_producto', as: 'Categoria_Producto' })

module.exports = {Productos}