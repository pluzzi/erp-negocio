const Sequelize = require('sequelize')
const { db } = require('../config/database')

var TiposCliente = db.sequelize.define(
    'tipos_cliente',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        tipo: {
            type: Sequelize.STRING
        }
    }, {
        freezeTableName: true
    }
)

module.exports = {TiposCliente}