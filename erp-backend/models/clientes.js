const Sequelize = require('sequelize')
const { db } = require('../config/database')
const { TiposCliente } = require('./tipos_cliente')

var Clientes = db.sequelize.define(
    'clientes',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        tipo_cliente: {
            type: Sequelize.INTEGER
        },
        nombre: {
            type: Sequelize.STRING
        },
        apellido: {
            type: Sequelize.STRING
        },
        razon_social: {
            type: Sequelize.STRING
        },
        taxid:{
            type: Sequelize.STRING
        }
    }, {
        freezeTableName: true
    }
)

Clientes.hasOne(TiposCliente, { foreignKey: 'id', sourceKey: 'tipo_cliente' , as: 'Tipo_Cliente' })

module.exports = {Clientes}