const Sequelize = require('sequelize')
const { db } = require('../config/database')
const { Presupuestos } = require('./presupuestos')

var Movimientos = db.sequelize.define(
    'movimientos',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        presupuesto: {
            type: Sequelize.INTEGER
        }
    }, {
        freezeTableName: true
    }
)

Movimientos.hasOne(Presupuestos, { foreignKey: 'id', sourceKey: 'presupuesto' , as: 'Presupuesto' })

module.exports = {Movimientos}