const Sequelize = require('sequelize')
const { db } = require('../config/database')
const { Perfiles } = require('./perfiles')

var Usuarios = db.sequelize.define(
    'usuarios',
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        nombre: {
            type: Sequelize.STRING
        },
        apellido: {
            type: Sequelize.STRING
        },
        email:{
            type: Sequelize.STRING
        },
        password: {
            type: Sequelize.STRING
        },
        perfil: {
            type: Sequelize.INTEGER
        }
    }, {
        freezeTableName: true
    }
)

Usuarios.hasOne(Perfiles, { foreignKey: 'id', sourceKey: 'perfil' , as: 'Perfil' })

module.exports = {Usuarios}

