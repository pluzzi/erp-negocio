const express = require('express')
const router = express.Router()
const { verifyToken, isAdmin } = require('../auth/auth')
const { Usuarios } = require('../../models/usuarios')
const { Perfiles } = require('../../models/perfiles')
const { db, getUpdateObject } = require('../../config/database')

router.route('/').get(verifyToken, isAdmin, async (req, res) => {
    try{
        let list =  await Usuarios.findAll({
            include: [
                {
                    model: Perfiles, as: "Perfil"
                }
            ]
        })

        res.send(list);
    }catch(err){
        res.status(500).send(err)
    }
})

router.route('/:id').get(verifyToken, isAdmin, async (req, res) => {
    try{
        const { id } = req.params

        let result = await Usuarios.findOne({
            where: {id: id},
            include: [
                {
                    model: Perfiles, as: "Perfil"
                }
            ]
        })

        if(result){
            res.send(result);
        }else{
            res.status(404).send('Usuario no encontrado.')
        }
    }catch(err){
        res.status(500).send(err)
    }
})

router.route('/:id').delete(verifyToken, isAdmin, async (req, res) => {
    try{
        const { id } = req.params

        let result = await Usuarios.findOne({
            where: {id: id}
        })

        if(result){
            db.sequelize.query("delete from usuarios where id = $1", { bind: [id] })
            res.send('Usuario borrado');
        }else{
            res.status(404).send('Usuario no encontrado.')
        }
          
    }catch(err){
        res.status(500).send(err)
    }
})

router.route('/:id').put(verifyToken, isAdmin, async (req, res) => {
    try{
        const { id } = req.params
        const data = req.body

        let usuario = await Usuarios.findOne({
            where: {id: id}
        })

        if(usuario){
            await usuario.update(getUpdateObject(Usuarios, data, ["id"]))
            res.send(usuario)
        }else{
            res.status(404).send('Usuario no encontrado.')
        }
          
    }catch(err){
        res.status(500).send(err)
    }
})

router.route('/').post(verifyToken, isAdmin, async (req, res) => {
    try{
        const data = req.body

        let usuario = await Usuarios.findOne({
            where: {email: data.email}
        })

        if(!usuario){
            const nuevo = await Usuarios.create(data)
            res.send(nuevo)
        }else{
            res.status(409).send('Usuario existente.')
        }
          
    }catch(err){
        res.status(500).send(err)
    }
})

module.exports = router