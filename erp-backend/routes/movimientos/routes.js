const express = require('express')
const router = express.Router()
const { verifyToken, isAdmin } = require('../auth/auth')
const { Movimientos } = require('../../models/movimientos')
const { Presupuestos } = require('../../models/presupuestos')
const { Clientes } = require('../../models/clientes')
const { PresupuestosDetalle } = require('../../models/presupuestos_detalle')
const { db, getUpdateObject } = require('../../config/database')

router.route('/').get(verifyToken, isAdmin, async (req, res) => {
    try{
        let result =  await Movimientos.findAll({
            include: [
                {
                    model: Presupuestos,
                    as: "Presupuesto",
                    include: [
                        {
                            model: Clientes, as: "Cliente"
                        },
                        {
                            model: PresupuestosDetalle, as: "Presupuesto_Detalle"
                        }
                    ]
                }
            ]
        })

        res.send(result);
    }catch(err){
        res.status(500).send(err)
    }
})

router.route('/:id').get(verifyToken, isAdmin, async (req, res) => {
    try{
        const { id } = req.params

        let result = await Movimientos.findOne({
            where: {id: id},
            include: [
                {
                    model: Presupuestos,
                    as: "Presupuesto",
                    include: [
                        {
                            model: Clientes, as: "Cliente"
                        },
                        {
                            model: PresupuestosDetalle, as: "Presupuesto_Detalle"
                        }
                    ]
                }
            ]
        })

        if(result){
            res.send(result);
        }else{
            res.status(404).send('Movimiento no encontrado.')
        }
    }catch(err){
        res.status(500).send(err)
    }
})

router.route('/:id').delete(verifyToken, isAdmin, async (req, res) => {
    try{
        const { id } = req.params

        let result = await Movimientos.findOne({
            where: {id: id}
        })

        if(result){
            db.sequelize.query("delete from movimientos where id = $1", { bind: [id] })
            res.send('Movimiento borrado');
        }else{
            res.status(404).send('Movimiento no encontrado.')
        }
          
    }catch(err){
        res.status(500).send(err)
    }
})

router.route('/:id').put(verifyToken, isAdmin, async (req, res) => {
    try{
        const { id } = req.params
        const data = req.body

        let result = await Movimientos.findOne({
            where: {id: id}
        })

        if(result){
            await result.update(getUpdateObject(Movimientos, data, ["id"]))
            res.send(result)
        }else{
            res.status(404).send('Movimiento no encontrado.')
        }
          
    }catch(err){
        res.status(500).send(err)
    }
})

router.route('/').post(verifyToken, isAdmin, async (req, res) => {
    try{
        const data = req.body

        let result = await Movimientos.findOne({
            where: {presupuesto: data.presupuesto}
        })

        if(!result){
            const nuevo = await Movimientos.create(data)
            res.send(nuevo)
        }else{
            res.status(409).send('Movimiento existente.')
        }
          
    }catch(err){
        res.status(500).send(err)
    }
})

module.exports = router