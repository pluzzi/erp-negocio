const express = require('express')
const router = express.Router()
const { verifyToken, isAdmin } = require('../auth/auth')
const { PresupuestosDetalle } = require('../../models/presupuestos_detalle')
const { Productos } = require('../../models/productos')
const { db, getUpdateObject } = require('../../config/database')

router.route('/').get(verifyToken, isAdmin, async (req, res) => {
    try{
        let result =  await PresupuestosDetalle.findAll({
            include: [
                {
                    model: Productos,
                    as: "Producto"
                }
            ]
        })
        res.send(result);
    }catch(err){
        res.status(500).send(err)
    }
})

router.route('/:presupuesto').get(verifyToken, isAdmin, async (req, res) => {
    try{
        const { presupuesto } = req.params

        let result = await PresupuestosDetalle.findOne({
            where: {presupuesto: presupuesto},
            include: [
                {
                    model: Productos,
                    as: "Producto"
                }
            ]
        })

        if(result){
            res.send(result);
        }else{
            res.status(404).send('Presupuesto no encontrado.')
        }
    }catch(err){
        res.status(500).send(err)
    }
})

router.route('/:id').delete(verifyToken, isAdmin, async (req, res) => {
    try{
        const { id } = req.params

        let result = await PresupuestosDetalle.findOne({
            where: {id: id}
        })

        if(result){
            db.sequelize.query("delete from presupuestos_detalle where id = $1", { bind: [id] })
            res.send('Presupuesto borrado');
        }else{
            res.status(404).send('Presupuesto no encontrado.')
        }
          
    }catch(err){
        res.status(500).send(err)
    }
})

router.route('/:id').put(verifyToken, isAdmin, async (req, res) => {
    try{
        const { id } = req.params
        const data = req.body

        let result = await PresupuestosDetalle.findOne({
            where: {id: id}
        })

        if(result){
            await result.update(getUpdateObject(PresupuestosDetalle, data, ["id"]))
            res.send(result)
        }else{
            res.status(404).send('Presupuesto no encontrado.')
        }
          
    }catch(err){
        res.status(500).send(err)
    }
})

router.route('/').post(verifyToken, isAdmin, async (req, res) => {
    try{
        const data = req.body

        const nuevo = await PresupuestosDetalle.create(data)
        res.send(nuevo)
          
    }catch(err){
        res.status(500).send(err)
    }
})

module.exports = router