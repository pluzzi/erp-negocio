const express = require('express')
const router = express.Router()
const { verifyToken, isAdmin } = require('../auth/auth')
const { Clientes } = require('../../models/clientes')
const { TiposCliente } = require('../../models/tipos_cliente')
const { db, getUpdateObject } = require('../../config/database')

router.route('/').get(verifyToken, isAdmin, async (req, res) => {
    try{
        let list =  await Clientes.findAll({
            include: [
                {
                    model: TiposCliente, as: "Tipo_Cliente"
                }
            ]
        })

        res.send(list);
    }catch(err){
        res.status(500).send(err)
    }
})

router.route('/:id').get(verifyToken, isAdmin, async (req, res) => {
    try{
        const { id } = req.params

        let perfil = await Clientes.findOne({
            where: {id: id},
            include: [
                {
                    model: TiposCliente, as: "Tipo_Cliente"
                }
            ]
        })

        if(perfil){
            res.send(perfil);
        }else{
            res.status(404).send('Cliente no encontrado.')
        }
    }catch(err){
        res.status(500).send(err)
    }
})

router.route('/:id').delete(verifyToken, isAdmin, async (req, res) => {
    try{
        const { id } = req.params

        let result = await Clientes.findOne({
            where: {id: id}
        })

        if(result){
            db.sequelize.query("delete from clientes where id = $1", { bind: [id] })
            res.send('Cliente borrado');
        }else{
            res.status(404).send('Cliente no encontrado.')
        }
          
    }catch(err){
        res.status(500).send(err)
    }
})

router.route('/:id').put(verifyToken, isAdmin, async (req, res) => {
    try{
        const { id } = req.params
        const data = req.body

        let result = await Clientes.findOne({
            where: {id: id}
        })

        if(result){
            await result.update(getUpdateObject(Clientes, data, ["id"]))
            res.send(result)
        }else{
            res.status(404).send('Cliente no encontrado.')
        }
          
    }catch(err){
        res.status(500).send(err)
    }
})

router.route('/').post(verifyToken, isAdmin, async (req, res) => {
    try{
        const data = req.body

        let result = await Clientes.findOne({
            where: {taxid: data.taxid}
        })

        if(!result){
            const nuevo = await Clientes.create(data)
            res.send(nuevo)
        }else{
            res.status(409).send('Cliente existente.')
        }
          
    }catch(err){
        res.status(500).send(err)
    }
})

module.exports = router