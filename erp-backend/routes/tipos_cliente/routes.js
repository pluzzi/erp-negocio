const express = require('express')
const router = express.Router()
const { verifyToken, isAdmin } = require('../auth/auth')
const { TiposCliente } = require('../../models/tipos_cliente')
const { db, getUpdateObject } = require('../../config/database')

router.route('/').get(verifyToken, isAdmin, async (req, res) => {
    try{
        let list =  await TiposCliente.findAll()

        res.send(list);
    }catch(err){
        res.status(500).send(err)
    }
})

router.route('/:id').get(verifyToken, isAdmin, async (req, res) => {
    try{
        const { id } = req.params

        let perfil = await TiposCliente.findOne({
            where: {id: id}
        })

        if(perfil){
            res.send(perfil);
        }else{
            res.status(404).send('Tipo de Cliente no encontrado.')
        }
    }catch(err){
        res.status(500).send(err)
    }
})

router.route('/:id').delete(verifyToken, isAdmin, async (req, res) => {
    try{
        const { id } = req.params

        let result = await TiposCliente.findOne({
            where: {id: id}
        })

        if(result){
            db.sequelize.query("delete from tipos_cliente where id = $1", { bind: [id] })
            res.send('Tipo de Cliente borrado');
        }else{
            res.status(404).send('Tipo de Cliente no encontrado.')
        }
          
    }catch(err){
        res.status(500).send(err)
    }
})

router.route('/:id').put(verifyToken, isAdmin, async (req, res) => {
    try{
        const { id } = req.params
        const data = req.body

        let result = await TiposCliente.findOne({
            where: {id: id}
        })

        if(result){
            await result.update(getUpdateObject(TiposCliente, data, ["id"]))
            res.send(result)
        }else{
            res.status(404).send('Tipo de Cliente no encontrado.')
        }
          
    }catch(err){
        res.status(500).send(err)
    }
})

router.route('/').post(verifyToken, isAdmin, async (req, res) => {
    try{
        const data = req.body

        let result = await TiposCliente.findOne({
            where: {tipo: data.tipo}
        })

        if(!result){
            const nuevo = await TiposCliente.create(data)
            res.send(nuevo)
        }else{
            res.status(409).send('Tipo de Cliente existente.')
        }
          
    }catch(err){
        res.status(500).send(err)
    }
})

module.exports = router