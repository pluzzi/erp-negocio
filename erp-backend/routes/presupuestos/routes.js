const express = require('express')
const router = express.Router()
const { verifyToken, isAdmin } = require('../auth/auth')
const { Presupuestos } = require('../../models/presupuestos')
const { Clientes } = require('../../models/clientes')
const { PresupuestosDetalle } = require('../../models/presupuestos_detalle')
const { db, getUpdateObject } = require('../../config/database')

router.route('/').get(verifyToken, isAdmin, async (req, res) => {
    try{
        let result =  await Presupuestos.findAll({
            include: [
                {
                    model: Clientes, as: "Cliente"
                },
                {
                    model: PresupuestosDetalle, as: "Presupuesto_Detalle"
                }
            ]
        })

        res.send(result);
    }catch(err){
        res.status(500).send(err)
    }
})

router.route('/:id').get(verifyToken, isAdmin, async (req, res) => {
    try{
        const { id } = req.params

        let result = await Presupuestos.findOne({
            where: {id: id},
            include: [
                {
                    model: Clientes, as: "Cliente"
                }
            ]
        })

        if(result){
            res.send(result);
        }else{
            res.status(404).send('Presupuesto no encontrado.')
        }
    }catch(err){
        res.status(500).send(err)
    }
})

router.route('/:id').delete(verifyToken, isAdmin, async (req, res) => {
    try{
        const { id } = req.params

        let result = await Presupuestos.findOne({
            where: {id: id}
        })

        if(result){
            db.sequelize.query("delete from presupuestos where id = $1", { bind: [id] })
            res.send('Presupuesto borrado');
        }else{
            res.status(404).send('Presupuesto no encontrado.')
        }
          
    }catch(err){
        res.status(500).send(err)
    }
})

router.route('/:id').put(verifyToken, isAdmin, async (req, res) => {
    try{
        const { id } = req.params
        const data = req.body

        let result = await Presupuestos.findOne({
            where: {id: id}
        })

        if(result){
            await result.update(getUpdateObject(Presupuestos, data, ["id"]))
            res.send(result)
        }else{
            res.status(404).send('Presupuesto no encontrado.')
        }
          
    }catch(err){
        res.status(500).send(err)
    }
})

router.route('/').post(verifyToken, isAdmin, async (req, res) => {
    try{
        const data = req.body
        const nuevo = await Presupuestos.create(data)
        res.send(nuevo)

    }catch(err){
        res.status(500).send(err)
    }
})

module.exports = router