drop table tipos_cliente

create table tipos_cliente(
	id int not null auto_increment,
    tipo varchar(255) not null,
	createdAt Datetime null,
    updatedAt Datetime null,
    primary key(id)
)