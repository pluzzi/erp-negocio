drop table presupuestos
drop table presupuestos_detalle

create table presupuestos(
	id int not null auto_increment,
    cliente int not null,
	createdAt Datetime null,
    updatedAt Datetime null,
    primary key(id)
)

create table presupuestos_detalle(
	id int not null auto_increment,
    presupuesto int not null,
    producto int not null,
    cantidad int not null,
    precio_unitario decimal(12,4) not null,
    total decimal(12,4) not null,
	createdAt Datetime null,
    updatedAt Datetime null,
    primary key(id)
)