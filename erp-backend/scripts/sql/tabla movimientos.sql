drop table movimientos

create table movimientos(
	id int not null auto_increment,
    presupuesto int not null,
	createdAt Datetime null,
    updatedAt Datetime null,
    primary key(id)
)